import Api from '@/services/api'

export default {
  changeCarOwner(key, newOwner) {
    return Api().put('cars', {       
      key: key,
      newOwner: newOwner
    })
  },
  createCar(key, make, model, color, owner) {
    return Api().post('cars', {  
      key: key,     
      make: make,
      model: model,
      color: color,
      owner: owner
    })
  },
  queryAllCars() {
    return Api().get('cars')
  }
}