var express = require('express')
var bodyParser = require('body-parser')
var query = require('./query');
var invoke = require('./invoke');

var app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/cars', async function (req, res) {

    if(req.query.key){
        
        const result = await query.getCar(req.query.key);
        res.send(JSON.parse(result));

    }else{      

    const result = await query.getAllCars();
    res.send(JSON.parse(result));

    }
})

app.post('/cars', async function (req, res) {

    console.log(req.body);
    data = req.body;

    const result = await invoke.createCar(data.key,data.make,data.model,data.color,data.owner);
    if(result) {
        res.send({message:"success"});
    }

})

app.put('/cars', async function (req, res) {

    console.log(req.body);
    data = req.body;

    const result = await invoke.changeCarOwner(data.key,data.newOwner);
    if(result) {
        res.send({message:"success"});
    }

})



 


app.listen(3000)